#include "network_common.h"
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <openssl/err.h>
#include <poll.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

int create_socket(int port, const char* addr, bool client) {
  int fd = socket(AF_INET, SOCK_STREAM, 0);
  if (fd < 0) {
    fprintf(stderr, "Unable to create socket, error: %s", strerror(errno));
    return -1;
  }
  if (fcntl(fd, F_SETFL, O_NONBLOCK) < 0) {
    fprintf(stderr, "Unable to set socket to non blocking mode, error: %s",
            strerror(errno));
    return -1;
  }
  struct sockaddr_in sock = {.sin_family = AF_INET,
                             .sin_addr.s_addr = inet_addr(addr),
                             .sin_port = htons(port)};
  if(client) {
    if (connect(fd, (struct sockaddr *)&sock, sizeof(sock)) < 0) {
      int e = errno;
      if (e == EINPROGRESS) {
	struct pollfd pollfd = {.fd = fd, .events = POLLOUT};
	poll(&pollfd, 1, -1);
	int option_value = -1;
	socklen_t option_len = sizeof(option_value);
	getsockopt(fd, SOL_SOCKET, SO_ERROR, &option_value, &option_len);
	if (option_value == 0) {
	  return fd;
	} // all good
      }
      fprintf(stderr, "Unable to connect to socket, error: %s", strerror(errno));
      close(fd);
      return -1;
    }
  } else {
    if (bind(fd, (struct sockaddr*)&sock, sizeof(sock)) < 0) {
      perror("Unable to bind");
      exit(EXIT_FAILURE);
    }
    if (listen(fd, 1) < 0) {
      perror("Unable to listen");
      exit(EXIT_FAILURE);
    }
  }
  return fd;
}

SSL_CTX *create_server_context() {
  const SSL_METHOD *method;
  SSL_CTX *ctx;

  method = TLS_server_method();

  ctx = SSL_CTX_new(method);
  if (!ctx) {
    fprintf(stderr, "Unable to create SSL context");
    ERR_print_errors_fp(stderr);
  }

  if (SSL_CTX_use_certificate_file(ctx, "cert.pem", SSL_FILETYPE_PEM) <= 0) {
    ERR_print_errors_fp(stderr);
    SSL_CTX_free(ctx);
    return NULL;
  }
  
  if (SSL_CTX_use_PrivateKey_file(ctx, "key.pem", SSL_FILETYPE_PEM) <= 0 ) {
    ERR_print_errors_fp(stderr);
    SSL_CTX_free(ctx);
    return NULL;
  }
  return ctx;
}

SSL_CTX *create_client_context() {
  const SSL_METHOD *method;
  SSL_CTX *ctx;

  method = TLS_client_method();

  ctx = SSL_CTX_new(method);
  if (!ctx) {
    fprintf(stderr, "Unable to create SSL context");
    ERR_print_errors_fp(stderr);
  }
  return ctx;
}

void ssl_handle_errors(SSL **ssl, int ret, int timeout) {
  if (ret > 0) return;
  int err = SSL_get_error(*ssl, ret);
  switch (err) {
  case SSL_ERROR_WANT_READ:
  case SSL_ERROR_WANT_WRITE:
    if (timeout != 0) {
      int fd = SSL_get_fd(*ssl);
      struct pollfd pollfd = {
	.fd = fd, .events = err == SSL_ERROR_WANT_READ ? POLLIN : POLLOUT};
      poll(&pollfd, 1, timeout);
    }
    break;
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wimplicit-fallthrough"
  case SSL_ERROR_SYSCALL:
    fprintf(stderr, "%s\n", strerror(errno));
  default:
#pragma GCC diagnostic pop
    fprintf(stderr, "Warning: couldn't connect to socket\n");
    close(SSL_get_fd(*ssl));
    SSL_free(*ssl);
    *ssl = 0;
    break;
  }
}

SSL* init_client_ssl(SSL_CTX *ctx, int port) {
  if (!ctx)
    return NULL;
  int fd = create_socket(port, "127.0.0.1", true);

  if (fd == -1) {
    SSL_CTX_free(ctx);
    return NULL;
  }

  SSL* ssl = SSL_new(ctx);
  SSL_CTX_free(ctx);
  SSL_set_fd(ssl, fd);

  while (ssl) {
    int ret = SSL_connect(ssl);
    if (ret != -1) {
      break;
    }
    // There's an error, let's see if it's just because
    // SSL_connect hasn't finished yet, or it's another error
    ssl_handle_errors(&ssl, ret, -1);
  }
  return ssl;
}

#ifndef NETWORK_COMMON_H
#define NETWORK_COMMON_H

#include <openssl/ssl.h>
#include <stdbool.h>

int create_socket(int port, const char* addr, bool client);
SSL_CTX *create_client_context();
SSL_CTX *create_server_context();
void ssl_handle_errors(SSL **ssl, int ret, int timeout);
SSL* init_client_ssl(SSL_CTX* ctx, int port);
#endif
